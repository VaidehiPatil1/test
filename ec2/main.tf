resource "aws_instance" "app" {
  ami           = var.ami
  instance_type = var.instance_type
  count         = var.instance_count
  subnet_id     = var.subnet_id
  
  tags = {
    application = "test"
  }
}
