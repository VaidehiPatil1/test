variable "ami" {
    description = "ami id"
    type = string
    default = "ami-089c6f2e3866f0f14"
}

variable "instance_type" {
    description = "instance_type id"
    type = string
    default = "t2.micro"
}

variable "instance_count" {
    description = "instance_count id"
    type = number
    default = 1
}

variable "subnet_id" {
    description = "subnet_id id"
    type = string
    default = "subnet-9fc591d3"
}
